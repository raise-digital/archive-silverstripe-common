<?php

namespace FDP\Common\XML;

class XMLDocument
{
    private $nodes;

    public function __construct($xml_path = null)
    {
        if (is_null($xml_path)) {
            $this->nodes = [];
        } else {
            $xml = new \XMLReader();
            $xml->open($xml_path);
            $this->nodes = $this->processXML($xml);
            $xml->close();
        }
    }

    public function __get($key)
    {
        switch ($key) {
            case 'nodes':
                return $this->nodes;
                break;
            default:
                return false;
                break;
        }
    }

    private function processXML($xml)
    {
        $nodes = [];
        while ($xml->read()) {
            switch ($xml->nodeType) {
                case \XMLReader::END_ELEMENT:
                    return $nodes;
                case \XMLReader::ELEMENT:
                    $node = new XMLNode();
                    $node->name = $xml->name;
                    if (!$xml->isEmptyElement) {
                        $new_content = $this->processXML($xml);
                    } else {
                        $new_content =[''];
                    }
                    foreach ($new_content as $new) {
                        $node->addContent($new);
                    }
                    if ($xml->hasAttributes) {
                        while ($xml->moveToNextAttribute()) {
                            $node->addAttr($xml->name, $xml->value);
                        }
                    }
                    $nodes[] = $node;
                    break;
                case \XMLReader::TEXT:
                case \XMLReader::CDATA:
                    $cdata_node = new XMLNode();
                    $cdata_node->cdata = true;
                    $cdata_node->addContent($xml->value);
                    $nodes[] = $cdata_node;
                    break;
            }
        }
        return $nodes;
    }

    public function addNode($node)
    {
        if (get_class($node) == XMLNode::class) {
            $this->nodes[] = $node;
        } else {
            return false;
        }
    }

    public function render($format = true)
    {
        $dom = new \DOMDocument();
        $dom->encoding = 'utf-8';
        $dom->formatOutput = $format;
        foreach ($this->nodes as $node) {
            if ($node->cdata) {
                $sub = $dom->createCDATASection($node->content);
                $dom->appendChild($sub);
            } elseif (is_null($node->name)) {
                $sub = $dom->createTextNode($node->content);
                $dom->appendChild($sub);
            } else {
                $sub = $dom->createElement($node->name);
                $sub = $node->render($dom, $sub);
                $dom->appendChild($sub);
            }
        }
        return $dom->saveXML();
    }
}
