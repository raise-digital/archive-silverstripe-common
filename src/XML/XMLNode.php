<?php

namespace FDP\Common\XML;

class XMLNode
{
    private $name;
    private $attrs = array();
    private $content;
    private $status;
    private $cdata = false;

    public function __construct($name = null)
    {
        $this->content = array();
        if (!is_null($name)) {
            $this->name = $name;
        }
        $this->status = 0;
    }

    public function __get($key)
    {
        switch ($key) {
            case 'name':
                return $this->name;
                break;
            case 'cdata':
                return $this->cdata;
                break;
            case 'attr':
                return $this->attrs;
                break;
            case 'content':
                if ($this->cdata) {
                    return implode('', $this->content);
                } elseif (count($this->content) == 1 && is_string($this->content[0])) {
                    return implode('', $this->content);
                } else {
                    return $this->content;
                }
                break;
            default:
                return false;
                break;
        }
    }

    public function __set($key, $value)
    {
        switch ($key) {
            case 'name':
                $this->name = $value;
                break;
            case 'cdata':
                $this->cdata = $value;
                break;
            default:
                return false;
                break;
        }
    }

    public function addAttr($id, $value)
    {
        $this->attrs[$id] = $value;
    }

    public function addContent($new_content, $index = false)
    {
        if ($index == false || (!isset($this->content[$index]) && $this->content[$index] != null)) {
            $this->content[] = $new_content;
            return;
        }
        $con_count = count($this->content);
        for ($x = $con_count-1; $x >= $index; $x = $x--) {
            $this->content[$x + 1] = $this->content[$x];
            $this->content = null;
        }
        $this->content[$index] = $new_content;
    }

    public function getAttr($attr_name)
    {
        if (isset($this->attrs[$attr_name])) {
            return $this->attrs[$attr_name];
        } else {
            return false;
        }
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function render($document, $element)
    {
        foreach ($this->attrs as $name => $value) {
            $attr = $element->setAttribute($name, $value);
        }
        foreach ($this->content as $child) {
            if (is_string($child)) {
                $sub = $document->createTextNode($child);
                $element->appendChild($sub);
            } elseif ($child->cdata) {
                $sub = $document->createCDATASection($child->content);
                $element->appendChild($sub);
            } else {
                $sub = $document->createElement($child->name);
                $sub = $child->render($document, $sub);
                $element->appendChild($sub);
            }
        }
        return $element;
    }
}
