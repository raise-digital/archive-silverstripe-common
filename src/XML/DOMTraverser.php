<?php

namespace FDP\Common\XML;

class DOMTraverser
{
    private $nodes;
    private $searches;
    private $currentSearch;

    public function __construct($nodes)
    {
        $this->nodes = $nodes;
        $this->searches = [];
        $this->currentSearch = [];
    }

    public function __get($key)
    {
        switch ($key) {
            case 'searches':
                return $this->searches;
                break;
            default:
                return false;
                break;
        }
    }

    public function search($dom_query)
    {
        if (!preg_match_all('/([a-zA-Z0-9_|\*]+)(\[[^\[^\]]*\])?/', $dom_query, $matches)) {
            return [];
        }
        $phases = [];
        for ($i = 0; $i < count($matches[0]); $i++) {
            $phase = ['name' => $matches[1][$i], 'attr' => []];
            preg_match_all("/([a-zA-Z0-9_]+)=\"([^\]^\"]*)\"/", $matches[2][$i], $attr);
            if (count($attr[0]) > 0) {
                for ($j = 0; $j < count($attr[0]); $j++) {
                    if (strlen(trim($attr[1][$j])) > 0) {
                        $attribute['name'] = $attr[1][$j];
                        $attribute['value'] = $attr[2][$j];
                        $phase['attr'][] = $attribute;
                    }
                }
            }
            $phases[] = $phase;
        }
        for ($i = 0; $i < count($matches[0]); $i++) {
            $this->currentSearch[$i] = [];
            $subject = ($i == 0) ? $this->nodes : $this->currentSearch[$i - 1];
            foreach ($subject as $node) {
                $this->checkNode($i, $node, $phases[$i]['name'], $phases[$i]['attr']);
            }
        }
        $this->searches[$dom_query] = $this->currentSearch[count($matches[0]) - 1];
        $this->currentSearch = [];
        return $this->searches[$dom_query];
    }

    private function checkNode($phase, $node, $search_name, $search_attr)
    {
        if ($search_name == '*' || $node->name == $search_name) {
            if (count($search_attr) > 0) {
                $matches = 0;
                foreach ($search_attr as $attr) {
                    if ($node->attr[$attr['name']] == $attr['value']) {
                        $matches += 1;
                    }
                }
                if ($matches == count($search_attr)) {
                    $this->currentSearch[$phase][] = $node;
                    return;
                }
            } else {
                $this->currentSearch[$phase][] = $node;
                return;
            }
        }
        if (is_array($node->content)) {
            foreach ($node->content as $child) {
                $this->checkNode($phase, $child, $search_name, $search_attr);
            }
        }
    }
}
