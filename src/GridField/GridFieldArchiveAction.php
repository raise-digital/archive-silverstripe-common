<?php

namespace FDP\Common\GridField;

use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridField_ActionProvider;
use SilverStripe\Forms\GridField\GridField_ColumnProvider;
use SilverStripe\Forms\GridField\GridField_FormAction;
use SilverStripe\ORM\DataObject;
use SilverStripe\ORM\ValidationException;

class GridFieldArchiveAction implements GridField_ColumnProvider, GridField_ActionProvider
{

    public function augmentColumns($gridField, &$columns)
    {
        if (!in_array('Actions', $columns)) {
            $columns[] = 'Actions';
        }
    }

    public function getColumnAttributes($gridField, $record, $columnName)
    {
        return array('class' => 'grid-field__col-compact');
    }

    public function getColumnMetadata($gridField, $columnName)
    {
        if ($columnName == 'Actions') {
            return array('title' => '');
        }
        return null;
    }

    public function getColumnsHandled($gridField)
    {
        return array('Actions');
    }

    public function getActions($gridField)
    {
        return array('archiverecord');
    }

    public function getColumnContent($grid, $obj, $column)
    {
        if (!$obj->canArchive()) {
            return null;
        }
        $action = GridField_FormAction::create(
            $grid,
            "ArchiveRecord{$obj->ID}",
            false,
            "archiverecord",
            ['RecordID' => $obj->ID]
        );
        $action->addExtraClass(
            'gridfield-button-archive btn--icon-md font-icon-trash-bin btn--no-text grid-field__icon-action'
        );
        $action->setAttribute('title', 'Archive');
        $action->setDescription('Archive');
        return $action->Field();
    }

    public function handleAction(GridField $gridField, $actionName, $arguments, $data)
    {
        if ($actionName == 'archiverecord') {
            $item = $gridField->getList()->byID($arguments['RecordID']);
            if (!$item || !$item->canArchive()) {
                throw new ValidationException('No archive permissions');
            }
            $item->doArchive();
        }
    }
}
