<?php

namespace FDP\Common\GridField;

use SilverStripe\Control\Controller;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridField_ActionProvider;
use SilverStripe\Forms\GridField\GridField_ColumnProvider;
use SilverStripe\Forms\GridField\GridField_FormAction;
use SilverStripe\Forms\GridField\GridFieldDetailForm;
use SilverStripe\ORM\DataObject;
use SilverStripe\ORM\ValidationException;

class GridFieldCopyAction implements GridField_ColumnProvider, GridField_ActionProvider
{

    public function augmentColumns($gridField, &$columns)
    {
        if (!in_array('Actions', $columns)) {
            $columns[] = 'Actions';
        }
    }

    public function getColumnAttributes($gridField, $record, $columnName)
    {
        return array('class' => 'grid-field__col-compact');
    }

    public function getColumnMetadata($gridField, $columnName)
    {
        if ($columnName == 'Actions') {
            return array('title' => '');
        }
        return null;
    }

    public function getColumnsHandled($gridField)
    {
        return array('Actions');
    }

    public function getActions($gridField)
    {
        return array('copyrecord');
    }

    public function getColumnContent($grid, $obj, $column)
    {
        if (!$obj->canArchive()) {
            return null;
        }
        $action = GridField_FormAction::create(
            $grid,
            "CopyRecord{$obj->ID}",
            false,
            "copyrecord",
            ['RecordID' => $obj->ID]
        );
        $action->addExtraClass(
            'gridfield-button-archive btn--icon-md font-icon-plus-circled btn--no-text grid-field__icon-action'
        );
        $action->setAttribute('title', 'Copy');
        $action->setDescription('Copy');
        return $action->Field();
    }

    public function handleAction(GridField $gridField, $actionName, $arguments, $data)
    {
        if ($actionName == 'copyrecord') {
            $item = $gridField->getList()->byID($arguments['RecordID']);
            if (!$item || !$item->canCreate()) {
                throw new ValidationException('No create permissions');
            }
            $item->duplicate();
        }
    }
}
