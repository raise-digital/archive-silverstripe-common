<?php

namespace FDP\Common\FieldType;

use FDP\Common\Forms\MultiEnumCheckboxSetField;

class DBMultiEnum extends \SilverStripe\ORM\FieldType\DBMultiEnum
{
    public function __construct($name = null, $enum = null, $default = null)
    {
        parent::__construct($name, $enum, $default);
    }

    public function formField($title = null, $name = null, $hasEmpty = false, $value = "", $emptyString = null)
    {
        if (!$title) {
            $title = $this->name;
        }
        if (!$name) {
            $name = $this->name;
        }
        return new MultiEnumCheckboxSetField($name, $title, $this->enumValues($hasEmpty), $value);
    }
}
