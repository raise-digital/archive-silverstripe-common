<?php

namespace FDP\Common\Extensions;

use FDP\Common\Utilities\Validator;

use SilverStripe\Core\Extension;

class AttributeValidatedDataObject extends Extension
{
    public function validateFieldsByAttributes($result, $fields)
    {
        foreach ($fields as $field) {
            $result = $this->validateFieldByAttributes($result, $field);
        }
        return $result;
    }

    public function validateFieldByAttributes($result, $field)
    {
        $attrs = $field->getAttributes();
        foreach ($attrs as $attr => $value) {
            $result = $this->owner->validateByAttribute($result, $field, $attr, $value, $attrs);
        }
        if ($field->hasMethod('getOptionAttributes')) {
            $option_attrs = $field->getOptionAttributes();
            foreach ($option_attrs as $attr => $value) {
                $result = $this->owner->validateByAttribute($result, $field, $attr, $value, $option_attrs);
            }
        }
        return $result;
    }

    public function validateByAttribute($result, $field, $attr, $value, $attrs)
    {
        $name = $field->getName();
        if (preg_match('/^data\-rule\-(.*?)$/', $attr, $match) && $value == 'true') {
            $valid = true;
            switch ($match[1]) {
                case 'required':
                    if (is_a($field, BooleanSetField::class)) {
                        $valid = isset($this->owner->{$name}) && !is_null($this->owner->{$name});
                    } else {
                        $valid = isset($this->owner->{$name}) && !empty($this->owner->{$name});
                    }
                    break;
                case 'email':
                    $val = $this->owner->{$name};
                    if (!empty($val)) {
                        $valid = Validator::email_address($val);
                    }
                    break;
                default:
                    if ($this->owner->hasMethod('validateByCustomAttribute')) {
                        $valid = $this->owner->validateByCustomAttribute($result, $field, $match[1]);
                    }
                    break;
            }
            if (!$valid) {
                $result->addFieldError($name, $attrs["data-msg-{$match[1]}"]);
            }
        }
        return $result;
    }
}
