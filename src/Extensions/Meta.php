<?php

namespace FDP\Common\Extensions;

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\ToggleCompositeField;
use SilverStripe\ORM\DataExtension;
use SilverStripe\View\HTML;

class Meta extends DataExtension
{
    private static $db = [
        'MetaTitle' => 'Varchar(200)',
        'MetaDescription' => 'Text',
        'MetaKeywords' => 'Text',
        'ExtraMeta' => "HTMLFragment(['whitelist' => ['meta', 'link']])",
        'MetaRobots' => 'Varchar(200)'
    ];
    private static $serialised_fields = ['MetaTitle', 'MetaDescription', 'MetaKeywords', 'MetaRobots'];

    public function updateCMSFields(FieldList $fields)
    {
        $fields->removeByName('MetaTitle');
        $fields->removeByName('MetaDescription');
        $fields->removeByName('MetaKeywords');
        $fields->removeByName('MetaRobots');
        $fields->removeByName('Metadata');
        $meta_fields = ToggleCompositeField::create('Metadata', 'Metadata', FieldList::create(
            TextField::create('MetaTitle', 'Title'),
            TextareaField::create('MetaDescription', 'Description')->setRows(3),
            TextareaField::create('MetaKeywords', 'Keywords')->setRows(3),
            TextareaField::create('ExtraMeta', 'Extra Tags')->setRows(30),
            TextField::create('MetaRobots', 'Robots')
        ))->setHeadingLevel(4);
        if ($fields->hasTabSet()) {
            $fields->addFieldToTab(
                'Root.Main',
                $meta_fields
            );
        } else {
            $fields->push($meta_fields);
        }
    }

    public function MetaTags(&$existing)
    {
        $tags = [];
        if (!empty($this->owner->MetaKeywords)) {
            $tags[] = HTML::createTag('meta', array(
                'name' => 'keywords',
                'content' => $this->owner->MetaKeywords,
            ));
        }
        if (!empty($this->owner->MetaRobots)) {
            $tags[] = HTML::createTag('meta', array(
                'name' => 'robots',
                'content' => $this->owner->MetaRobots,
            ));
        }
        $existing .= implode("\n", $tags);
    }
}
