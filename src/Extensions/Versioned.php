<?php

namespace FDP\Common\Extensions;

use SilverStripe\Core\Extension;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\ORM\FieldType\DBHTMLText;

class Versioned extends Extension
{
    public function VersionedStatus()
    {
        if ($this->owner->latestPublished() && $this->owner->Version > 0) {
            return DBField::create_field(DBHTMLText::class, '<span style="color: rgb(48, 191, 19)">Published</span>');
        } else {
            return DBField::create_field(DBHTMLText::class, '<span style="color: rgb(227, 49, 49)">Draft</span>');
        }
    }
}
