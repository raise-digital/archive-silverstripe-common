<?php

namespace FDP\Common\Forms;

class FileField extends \SilverStripe\Forms\FileField
{
    public function getAttributes()
    {
        $attributes = parent::getAttributes();
        if (array_key_exists('value', $attributes)) {
            unset($attributes['value']);
        }
        return $attributes;
    }
}
