<?php

namespace FDP\Common\Forms;

use SilverStripe\Forms\CheckboxSetField;
use SilverStripe\ORM\DataObjectInterface;

class MultiEnumCheckboxSetField extends CheckboxSetField
{
    public function setValue($value, $data = null)
    {
        if (!isset($value)) {
            $this->value = null;
        } elseif (is_string($value)) {
            $this->value = explode(',', $value);
        } else {
            $this->value = $value;
        }
    }

    public function saveInto(DataObjectInterface $record)
    {
        $record->{$this->name} = implode(',', $this->value);
    }
}
