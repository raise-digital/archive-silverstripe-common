<?php

namespace FDP\Common\Traits;

use SilverStripe\Assets\File;
use SilverStripe\Assets\Folder;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Dev\FixtureBlueprint;
use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\DataExtension;
use SilverStripe\ORM\DataObject;
use SilverStripe\Security\RandomGenerator;
use SilverStripe\Versioned\Versioned;

trait TestUtilities
{

    private static $test_files = [
        'jpg' => [
            'path' => 'image.jpg',
            'type' => 'image/jpg'
        ],
        'png' => [
            'path' => 'image.png',
            'type' => 'image/png'
        ],
        'pdf' => [
            'path' => 'document.pdf',
            'type' => 'application/pdf'
        ]
    ];

    private $registered = false;

    protected function shouldSetupDatabaseForCurrentTest($files)
    {
        if (parent::shouldSetupDatabaseForCurrentTest($files)) {
            if (!$this->registered) {
                $factory = $this->getFixtureFactory();
                foreach (static::get_blueprints() as $type => $blueprint) {
                    $factory->define($type, $blueprint);
                }
            }
            return true;
        }
        return false;
    }

    public static function get_blueprint(...$args)
    {
        $type = $args[0];
        if (count($args) == 2) {
            switch ($args[1]) {
                case 'Publish':
                    $when = 'afterCreate';
                    $callback = function ($obj, $id, $data, $fixtures) {
                        $obj->copyVersionToStage(Versioned::DRAFT, Versioned::LIVE);
                    };
                    break;
                case 'File':
                    $when = 'afterCreate';
                    $callback = function ($obj, $id, $data, $fixtures) {
                        $extension = $obj->getExtension();
                        if (array_key_exists($extension, self::$test_files)) {
                            $obj->setFromLocalFile(implode(
                                DIRECTORY_SEPARATOR,
                                [BASE_PATH, self::$test_files[$extension]['path']]
                            ), $obj->Name);
                            $obj->write();
                        }
                        $obj->copyVersionToStage(Versioned::DRAFT, Versioned::LIVE);
                    };
                    break;
                case 'Replace':
                    $when = 'beforeCreate';
                    $callback = function ($id, $data, $fixtures) use ($type) {
                        foreach (DataObject::get($type) as $obj) {
                            $obj->delete();
                        }
                    };
                    break;
            }
        } else {
            $when = $args[1];
            $callback = $args[2];
        }
        return Injector::inst()->create(FixtureBlueprint::class, $type)->addCallback($when, $callback);
    }

    public function generateFileUpload($name)
    {
        $extension = File::get_file_extension($name);
        if (array_key_exists($extension, static::$test_files)) {
            $path = implode(
                DIRECTORY_SEPARATOR,
                [__DIR__, '../../tests/resources', static::$test_files[$extension]['path']]
            );
            return [
                'name' => $name,
                'type' => static::$test_files[$extension]['type'],
                'tmp_name' => $path,
                'error' => 0,
                'size' => filesize($path)
            ];
        }
        return [];
    }

    public function checkPageHeader($page)
    {
        if (is_string($page)) {
            $page = DataObject::get($page)->first();
        }
        $response = $this->get($page->Link());
        $this->assertEquals(200, $response->getStatusCode());
        if ($page->MetaTitle) {
            $this->assertExactMatchBySelector('title', [$page->MetaTitle]);
        } else {
            $this->assertExactMatchBySelector('title', [$page->Title]);
        }
        return $response;
    }

    public function assertCanEdit($obj)
    {
        if (is_string($obj)) {
            $obj = DataObject::get($obj)->first();
        }
        $this->assertInstanceOf(FieldList::class, $obj->getCMSFields());
        $this->assertInstanceOf(FieldList::class, $obj->getCMSActions());
    }

    public function assertArrayHasKeys($keys, $array)
    {
        $this->assertInternalType('array', $array);
        foreach ($keys as $key) {
            $this->assertArrayHasKey($key, $array);
        }
    }

    public function assertArrayNotHasKeys($keys, $array)
    {
        $this->assertInternalType('array', $array);
        foreach ($keys as $key) {
            $this->assertArrayNotHasKey($key, $array);
        }
    }

    public function assertArrayHasOnlyKeys($keys, $array)
    {
        $this->assertArrayHasKeys($keys, $array);
        $this->assertEquals(count($keys), count($array));
    }

    public function assertArrayKeysContainString($string, $array)
    {
        $matched = false;
        foreach ($array as $k => $v) {
            if (strpos($k, $string) !== false) {
                $matched = true;
                break;
            }
        }
        $this->assertTrue($matched, "Array keys do not contain text '{$string}'");
    }

    public function assertNotArrayKeysContainString($string, $array)
    {
        $matched = false;
        foreach ($array as $k => $v) {
            if (strpos($k, $string) !== false) {
                $matched = true;
                break;
            }
        }
        $this->assertFalse($matched, "Array keys (" . implode(', ', array_keys($array)) . ") contain text '{$string}'");
    }

    public function assertValidURL($value)
    {
        $this->assertTrue(
            filter_var($value, FILTER_VALIDATE_URL) !== false,
            "Failed asserting that '{$value}' is a valid URL"
        );
    }

    public function assertValidLink($link)
    {
        $response = $this->get($link);
        $this->assertEquals(
            200,
            $response->getStatusCode(),
            "Failed asserting that '{$link}' is a valid internal link"
        );
    }

    public function assertValidationResultHasMessages($messages, $result)
    {
        foreach ($result->getMessages() as $message) {
            if (($index = array_search($message['fieldName'], $messages)) !== false) {
                unset($messages[$index]);
            }
        }
        $this->assertEquals(
            0,
            count($messages),
            sprintf('Failed to assert validation result contains "%s"', implode(', ', $messages))
        );
    }
}
