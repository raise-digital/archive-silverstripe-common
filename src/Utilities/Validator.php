<?php

namespace FDP\Common\Utilities;

class Validator
{
    public static function email_address($value)
    {
        $pattern = '^[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*' .
                   '@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$';
        $safe = str_replace('/', '\\/', $pattern);
        if ($value && !preg_match('/' . $safe . '/i', $value)) {
            return false;
        }
        return true;
    }

    public static function date($value, $format)
    {
        switch ($format) {
            case 'd/m/Y':
            default:
                if (preg_match('/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/', $value, $parts)) {
                    return checkdate($parts[2], $parts[1], $parts[3]);
                } else {
                    return false;
                }
                break;
        }
    }
}
