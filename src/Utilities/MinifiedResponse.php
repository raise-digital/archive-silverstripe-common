<?php

namespace FDP\Common\Utilities;

use SilverStripe\Control\HTTPResponse;

use zz\Html\HTMLMinify;

class MinifiedResponse extends HTTPResponse
{
    protected function outputBody()
    {
        if (!$this->isError()) {
            $this->setBody(HTMLMinify::minify(
                $this->getBody(),
                array('optimizationLevel' => HTMLMinify::OPTIMIZATION_ADVANCED)
            ));
        }
        return parent::outputBody();
    }
}
