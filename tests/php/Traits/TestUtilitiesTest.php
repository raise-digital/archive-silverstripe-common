<?php

namespace FDP\Common\Tests\Traits;

use FDP\Common\Tests\TestModels\ReplaceableTestModel;
use FDP\Common\Tests\TestModels\VersionedTestModel;
use FDP\Common\Traits\TestUtilities;

use SilverStripe\Assets\Image;
use SilverStripe\Dev\FunctionalTest;
use SilverStripe\ORM\ValidationResult;

class TestUtilitiesTest extends FunctionalTest
{
    use TestUtilities;

    protected static $extra_dataobjects = [
        ReplaceableTestModel::class,
        VersionedTestModel::class
    ];

    public static function get_fixture_file()
    {
        return ['../Traits/TestUtilitiesTest.yml'];
    }

    public static function get_blueprints()
    {
        return [
            \Page::class => self::get_blueprint(\Page::class, 'Publish')
        ];
    }

    public function testBlueprints()
    {
        $published = self::get_blueprint(VersionedTestModel::class, 'Publish');
        $this->assertNotNull($published, 'Published blueprint is null');
        $file = self::get_blueprint(Image::class, 'File');
        $this->assertNotNull($file, 'File blueprint is null');
        $replaced = self::get_blueprint(ReplaceableTestModel::class, 'Replace');
        $this->assertNotNull($replaced, 'Replaced blueprint is null');
        $unreciped = self::get_blueprint(\Page::class, 'beforeCreate', function ($id, $data, $fixtures){});
    }

    public function testFileUpload()
    {
        $valid = $this->generateFileUpload('test.jpg');
        $this->assertNotEquals(0, count($valid));
        $invalid = $this->generateFileUpload('test.xml');
        $this->assertEquals(0, count($invalid));
    }

    public function testPageHeader()
    {
        $response = $this->checkPageHeader(\Page::class);
        $response = $this->checkPageHeader($this->objFromFixture(\Page::class, 'meta'));
    }

    public function testCanEdit()
    {
        $this->assertCanEdit(\Page::class);
    }

    public function testArrayAssertions()
    {
        $this->assertArrayHasKeys(['a', 'b', 'c'], ['a' => 1, 'b' => 2, 'c' => 3]);
        $this->assertArrayNotHasKeys(['d'], ['a' => 1, 'b' => 2, 'c' => 3]);
        $this->assertArrayHasOnlyKeys(['a', 'b', 'c'], ['a' => 1, 'b' => 2, 'c' => 3]);
        $this->assertArrayKeysContainString('successful', ['successful-test' => 1, 'failed-test' => 2]);
        $this->assertNotArrayKeysContainString('abcd', ['successful-test' => 1, 'failed-test' => 2]);
    }

    public function testLinkAssertions()
    {
        $this->assertValidURL('https://www.fdpgroup.co.uk');
        $this->assertValidLink($this->objFromFixture(\Page::class, 'standard')->Link());
    }

    public function testValidation()
    {
        $result = new ValidationResult();
        $result->addFieldError('FieldA', 'Please enter field A');
        $result->addFieldError('FieldB', 'Please enter field B');
        $this->assertValidationResultHasMessages(['FieldA', 'FieldB'], $result);
    }
}
