<?php

namespace FDP\Common\Tests\GridField;

use FDP\Common\GridField\GridFieldArchiveAction;
use FDP\Common\Tests\TestModels\ArchivableTestModel;
use FDP\Common\Traits\TestUtilities;

use SilverStripe\Control\Controller;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\Dev\CSSContentParser;
use SilverStripe\Dev\SapphireTest;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\Form;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig;
use SilverStripe\Forms\GridField\GridFieldEditButton;
use SilverStripe\ORM\ValidationException;
use SilverStripe\Security\SecurityToken;

class GridFieldArchiveActionTest extends SapphireTest
{
    use TestUtilities;

    protected static $extra_dataobjects = [
        ArchivableTestModel::class
    ];

    public static function get_fixture_file()
    {
        return ['../GridField/GridFieldArchiveActionTest.yml'];
    }

    public static function get_blueprints()
    {
        return [ArchivableTestModel::class => self::get_blueprint(ArchivableTestModel::class, 'Publish')];
    }

    protected $list;
    protected $grid;
    protected $form;

    public function setUp()
    {
        parent::setUp();
        $this->list = ArchivableTestModel::get();
        $this->grid = GridField::create(
            'TestGrid',
            'TestGrid',
            $this->list,
            GridFieldConfig::create()->addComponents(
                new GridFieldEditButton(),
                new GridFieldArchiveAction()
            )
        );
        $this->form = new Form(
            null,
            'testingform',
            FieldList::create($this->grid),
            FieldList::create()
        );
    }

    public function testArchiveColumnProperties()
    {
        $component = $this->grid->getConfig()->getComponentByType(GridFieldArchiveAction::class);

        $columns = [];
        $component->augmentColumns($this->grid, $columns);
        $this->assertContains('Actions', $columns);
        $component->augmentColumns($this->grid, $columns);
        $this->assertEquals(1, count($columns));

        $this->assertArrayHasKey('title', $component->getColumnMetaData($this->grid, 'Actions'));
        $this->assertNull($component->getColumnMetaData($this->grid, ''));

        $this->assertNotEmpty($component->getColumnContent(
            $this->grid,
            $this->objFromFixture(ArchivableTestModel::class, 'archivable'),
            'Actions'
        ));
    }

    public function testArchiveButtonVisible()
    {
        $this->logInWithPermission('ADMIN');
        $content = new CSSContentParser($this->grid->FieldHolder());
        $buttons = $content->getBySelector('.gridfield-button-archive');
        $this->assertEquals(
            $this->list->filter('Archivable', true)->count(),
            count($buttons),
            'Archive buttons should show when logged in.'
        );
    }

    public function testArchiveButtonCanArchive()
    {
        $total = $this->list->count();
        $this->logInWithPermission('ADMIN');
        $stateID = 'testGridStateActionField';
        $session = Controller::curr()->getRequest()->getSession();
        $session->set(
            $stateID,
            array(
                'grid'=>'',
                'actionName'=>'archiverecord',
                'args' => array(
                    'RecordID' => $this->idFromFixture(ArchivableTestModel::class, 'archivable')
                )
            )
        );
        $token = SecurityToken::inst();
        $request = new HTTPRequest(
            'POST',
            'url',
            array(),
            array(
                'action_gridFieldAlterAction?StateID=' . $stateID=>true,
                $token->getName() => $token->getValue(),
            )
        );
        $request->setSession($session);
        $this->grid->gridFieldAlterAction(['StateID' => $stateID], $this->form, $request);
        $this->assertEquals($total - 1, $this->list->count(), 'User can not archive records');
    }

    public function testArchiveButtonCanNotArchive()
    {
        $total = $this->list->count();
        $this->logInWithPermission('ADMIN');
        $stateID = 'testGridStateActionField';
        $session = Controller::curr()->getRequest()->getSession();
        $session->set(
            $stateID,
            array(
                'grid'=>'',
                'actionName'=>'archiverecord',
                'args' => array(
                    'RecordID' => $this->idFromFixture(ArchivableTestModel::class, 'unarchivable')
                )
            )
        );
        $token = SecurityToken::inst();
        $this->expectException(ValidationException::class);
        $request = new HTTPRequest(
            'POST',
            'url',
            array(),
            array(
                'action_gridFieldAlterAction?StateID=' . $stateID=>true,
                $token->getName() => $token->getValue(),
            )
        );
        $request->setSession($session);
        $this->grid->gridFieldAlterAction(['StateID' => $stateID], $this->form, $request);
        $this->assertEquals($total, $this->list->count(), 'User can archive non-archivable records');
    }
}
