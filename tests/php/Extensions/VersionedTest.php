<?php

namespace FDP\Common\Tests\Extensions;

use FDP\Common\Tests\TestModels\VersionedTestModel;
use FDP\Common\Traits\TestUtilities;

use SilverStripe\Dev\SapphireTest;
use SilverStripe\Versioned\Versioned;

class VersionedTest extends SapphireTest
{
    use TestUtilities;

    protected static $extra_dataobjects = [
        VersionedTestModel::class
    ];

    public static function get_fixture_file()
    {
        return ['../Extensions/VersionedTest.yml'];
    }

    public static function get_blueprints()
    {
        return [];
    }

    public function testViewVersioned()
    {
        $versioned = $this->objFromFixture(VersionedTestModel::class, 'standard');
        $this->assertRegExp('/Draft/', $versioned->VersionedStatus()->forTemplate());
        $versioned->copyVersionToStage(Versioned::DRAFT, Versioned::LIVE);
        $this->assertRegExp('/Published/', $versioned->VersionedStatus()->forTemplate());
    }
}
