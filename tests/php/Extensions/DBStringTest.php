<?php

namespace FDP\Common\Tests\Extensions;

use FDP\Common\Traits\TestUtilities;

use SilverStripe\Dev\SapphireTest;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\ORM\FieldType\DBVarchar;

class DBStringTest extends SapphireTest
{
    use TestUtilities;

    public function testStringSlugify()
    {
        $this->assertEquals(
            '123-string-and-has-characters',
            DBField::create_field(DBVarchar::class, '123 string & has £@($%(@(@ characters')->Slugify()
        );
    }
}
