<?php

namespace FDP\Common\Tests\Extensions;

use FDP\Common\Tests\TestModels\MetaTestModel;
use FDP\Common\Traits\TestUtilities;

use SilverStripe\Dev\SapphireTest;

class MetaTest extends SapphireTest
{
    use TestUtilities;

    protected static $extra_dataobjects = [
        MetaTestModel::class
    ];

    public static function get_fixture_file()
    {
        return ['../Extensions/MetaTest.yml'];
    }

    public static function get_blueprints()
    {
        return [];
    }

    public function testViewMeta()
    {
        $meta = $this->objFromFixture(MetaTestModel::class, 'standard');
        $this->assertNotEmpty($meta->MetaTags(''));
    }

    public function testEditMeta()
    {
        $this->assertCanEdit(MetaTestModel::class);
        $meta = $this->objFromFixture(MetaTestModel::class, 'standard');
    }
}
