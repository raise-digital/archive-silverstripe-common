<?php

namespace FDP\Common\Tests\Utilities;

use FDP\Common\Traits\TestUtilities;
use FDP\Common\Utilities\Requirements_Backend;

use InvalidArgumentException;
use SilverStripe\Control\Director;
use SilverStripe\Core\Kernel;
use SilverStripe\Core\Config\Config;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Dev\SapphireTest;

class Requirements_BackendTest extends SapphireTest
{
    use TestUtilities;

    public function testRequirementsBackend()
    {
        $kernel = Injector::inst()->get(Kernel::class);
        $environment = $kernel->getEnvironment();
        if ($environment != 'dev') {
            $kernel->setEnvironment('dev');
        }

        $backend = Injector::inst()->create(Requirements_Backend::class);

        $name = 'TestPage.js';
        $backend->themedJavascript($name, 'application/javascript');
        $this->assertArrayKeysContainString($name, $backend->getJavascript());
        $backend->clear();

        $replacement = 'TestAnotherPage.js';
        $backend->themedJavascript($replacement, 'application/javascript');
        $this->assertArrayKeysContainString($replacement, $backend->getJavascript());
        $this->assertNotArrayKeysContainString($name, $backend->getJavascript());
        $backend->clear();

        $kernel->setEnvironment('test');
        $backend = Injector::inst()->create(Requirements_Backend::class);
        $this->expectException(InvalidArgumentException::class);
        $backend->themedJavascript($name, 'application/javascript');

        if ($environment != 'dev') {
            $kernel->setEnvironment($environment);
        }
    }
}
