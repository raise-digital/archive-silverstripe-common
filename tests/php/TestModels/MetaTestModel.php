<?php

namespace FDP\Common\Tests\TestModels;

use FDP\Common\Extensions\Meta;

use SilverStripe\Dev\TestOnly;
use SilverStripe\ORM\DataObject;

class MetaTestModel extends DataObject implements TestOnly
{
    private static $table_name = 'MetaTestModel';
    private static $extensions = [
        Meta::class
    ];

    private static $db = [
        'Title' => 'Varchar(200)'
    ];
}
