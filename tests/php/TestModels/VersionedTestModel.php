<?php

namespace FDP\Common\Tests\TestModels;

use SilverStripe\Dev\TestOnly;
use SilverStripe\ORM\DataObject;

class VersionedTestModel extends DataObject implements TestOnly
{
    private static $table_name = 'VersionedTestModel';
    private static $extensions = [
        \SilverStripe\Versioned\Versioned::class,
        \FDP\Common\Extensions\Versioned::class
    ];
    private static $db = [
        'Title' => 'Varchar(200)'
    ];
}
