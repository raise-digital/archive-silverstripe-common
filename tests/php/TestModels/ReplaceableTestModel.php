<?php

namespace FDP\Common\Tests\TestModels;

use SilverStripe\Dev\TestOnly;
use SilverStripe\ORM\DataObject;

class ReplaceableTestModel extends DataObject implements TestOnly
{
    private static $table_name = 'ReplaceableTestModel';
    private static $db = [
        'Name' => 'Varchar(200)'
    ];
}
